#mainly copy pasted from https://github.com/Tony607/ROC-Keras/blob/master/ROC-Keras.ipynb THX

#for NN classifer
import numpy as np
from keras.models import Sequential
from keras.layers.core import Dense
from keras.regularizers import L1L2

#For ROC curve
from sklearn.metrics import roc_curve
#For AUC
from sklearn.metrics import auc
#to draw ROC curve
import matplotlib.pyplot as plt

#for random forest classifer - for fun to compare with NN
from sklearn.ensemble import RandomForestClassifier

#To save the decision tree and use it in the main program
import pickle

#for decision tree
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score, recall_score

#to find the best param of the decision tree
from sklearn.model_selection import GridSearchCV

#for timestamp in filename
import datetime


#https://visualstudiomagazine.com/articles/2018/08/30/neural-binary-classification-keras.aspx
#Load TEST and TRAIN data
aTrainData = "mlDataTrain.csv"
aTestData = "mlDataTest.csv"

#Data have 4 fields 
atrainDataX = np.loadtxt(aTrainData, delimiter=';',usecols=[1,2,3,4], dtype=int)
atrainDataY = np.loadtxt(aTrainData, delimiter=';',usecols=[0], dtype=int) 

atestDataX = np.loadtxt(aTestData, delimiter=';', usecols=[1,2,3,4], dtype=int)
atestDataY =np.loadtxt(aTestData, delimiter=';', usecols=[0], dtype=int)

#to try on the different algo - last refresh 3 days ago, 500 members in chat group, latest message date 2 days ago, 50 messages in the last 7 days
aOneTestCase=[[3, 500, 2, 20]]

################################################################
################################ KERAS NN
################################################################

#Build model
aKerasNnModel = Sequential()
aKerasNnModel.add(Dense(4, input_dim=4, activation='relu'))
#aKerasNnModel.add(Dense(4, activation='relu'))
aKerasNnModel.add(Dense(1, activation='sigmoid'))

#Compile it
aKerasNnModel.compile(optimizer='adam',loss='binary_crossentropy',metrics=['accuracy'])
#Train it
aKerasNnModel.fit(atrainDataX, atrainDataY, epochs=50,verbose = 1)
#evaluate
aKerasNnModelScore = aKerasNnModel.evaluate(atestDataX, atestDataY)
print("NN algorithm results: {0} for folowwing metrics : {1}".format(aKerasNnModelScore,aKerasNnModel.metrics_names))

#Make a prediction
aKerasNnModelSinglePrediction = np.array(aOneTestCase, dtype=int)
aKerasNnModelSinglePredictionResults = aKerasNnModel.predict(aKerasNnModelSinglePrediction)
print("NN prediction for the single test case: {0}".format(aKerasNnModelSinglePredictionResults))

#Compute ROC for Keras NN model (for fun)
atestDataYPredictedKeras = aKerasNnModel.predict(atestDataX).ravel()
aFprNnKeras, aTprNnKeras, aThresholdsNnKeras = roc_curve(atestDataY, atestDataYPredictedKeras)

aNbSteps = len(aFprNnKeras)
for aCurrentStepNb in range(aNbSteps):
    #print("Step: {0},aFprNnKeras: {1}, aTprNnKeras: {2}, aThresholdsNnKeras: {3}".format(aCurrentStepNb,aFprNnKeras[aCurrentStepNb],aTprNnKeras[aCurrentStepNb],aThresholdsNnKeras[aCurrentStepNb]))
    pass

#Compute AUC (for extra fun)
aAucKeras = auc(aFprNnKeras, aTprNnKeras)
#Drawn it
plt.figure(1)
plt.plot([0, 1], [0, 1], 'k--')
plt.plot(aFprNnKeras, aTprNnKeras, label='Keras (area = {:.3f})'.format(aAucKeras))
plt.xlabel('False positive rate')
plt.ylabel('True positive rate')
plt.title('ROC curve')

#fig, ax = plt.subplots()
#ax.scatter(fpr_keras, tpr_keras)
#for i, txt in enumerate(thresholds_keras):
#    if (i % 5 == 0):
#        ax.annotate(txt, (fpr_keras[i], tpr_keras[i]))

#Save the model to use it in the program
aKerasNnModel.save("model.h5")

################################################################
################################ Rand Forest
################################################################

## Do the same with another ML algorithm used to classify
#for fun we do a random forest classifier to compare with kears
aRandomForstModel = RandomForestClassifier(max_depth=3, n_estimators=10)
aRandomForstModel.fit(atrainDataX, atrainDataY)

#evaluate
aRandomForstPredictions = aRandomForstModel.predict(atestDataX)
aRandomForstAcc = accuracy_score(atestDataY, aRandomForstPredictions)
aRandomForstRecall = recall_score(atestDataY, aRandomForstPredictions)
print("Random Forest algorithm results: Accuracy: {0} and recall: {1}".format(aRandomForstAcc,aRandomForstRecall))

#Compute ROC for randome forest classifier (for fun)
atestDataYPredictedRandomForest = aRandomForstModel.predict_proba(atestDataX)[:, 1]
aFprRandomForest, aTprRandomForest, aThresholdsRandomForest = roc_curve(atestDataY, atestDataYPredictedRandomForest)
aAucRandomForstModel = auc(aFprRandomForest, aTprRandomForest)

#test single prediction 
atestDataYPredictedRandomForest = aRandomForstModel.predict(aOneTestCase)
print("RF prediction for the single test case: {0}".format(atestDataYPredictedRandomForest))

#Drawn it to the plot 
#plt.plot(fpr_rf, tpr_rf, label='RF (area = {:.3f})'.format(aAucRandomForstModel))
plt.plot(aFprRandomForest, aTprRandomForest, label='Rand Forest (area = {:.3f})'.format(aAucRandomForstModel))
plt.legend()

#plt.show() #We run in a docker so let s rather save it as PNG to see it on the host
aDateTime = datetime.datetime.now().strftime("%m-%d_%H-%M-%S")
aRocsFilename = 'ROC_{0}.png'.format(aDateTime)
plt.savefig(aRocsFilename)

################################################################
################################ Decision Tree
################################################################

#and again a different algo - decision tree
aFeatures = ['dow', 'nbuser', 'nbdays','nbMsgLastWeek']
aDecisionTreeClassifier = DecisionTreeClassifier()
aDecisionTreeClassifier.fit(atrainDataX, atrainDataY)
aDecisionTreePredictions = aDecisionTreeClassifier.predict(atestDataX)
aAccuracy = accuracy_score(atestDataY, aDecisionTreePredictions)
aRecall = recall_score(atestDataY, aDecisionTreePredictions)
print("Decision tree algorithm results: Accuracy: {0} and recall: {1}".format(aAccuracy,aRecall))

#Compute ROC for randome forest classifier (for fun)
atestDataYPredictedDecisionTree = aDecisionTreeClassifier.predict_proba(atestDataX)[:, 1]
aFprDecisionTree, aTprDecisionTree, aThresholdsDecisionTree = roc_curve(atestDataY, atestDataYPredictedDecisionTree)
aAucDecisionTreeModel = auc(aFprDecisionTree, aTprDecisionTree)

#Drawn it to the plot 
plt.plot(aFprDecisionTree, aTprDecisionTree, label='Decision Tree (area = {:.3f})'.format(aAucDecisionTreeModel))
plt.legend()

#Predict for test case
aTestPred = aDecisionTreeClassifier.predict_proba(aOneTestCase)
print("Decision Tree prediction for the single test case: {0}".format(aTestPred))

#Now let s try to refine the Decision tree by trying several parameters
aGridSearchParams = {'max_features': [None, 'sqrt'],'max_depth' : [3, 5, 10, None],'min_samples_leaf': [1, 2, 3, 5, 10],'min_samples_split': [2, 4, 8, 16],'max_leaf_nodes': [10, 20, 50, 100, 500, 1000]}

# instantiate the grid
aGridSearchResult = GridSearchCV(DecisionTreeClassifier(), aGridSearchParams, cv=5, )

# fit the grid with data
aGridSearchResult.fit(atrainDataX, atrainDataY)

#let s see how good it is
aDecisionTreeRefinedPrediction = aGridSearchResult.best_estimator_.predict(atestDataX)
aAccuracyRef = accuracy_score(atestDataY, aDecisionTreeRefinedPrediction)
aRecallRef = recall_score(atestDataY, aDecisionTreeRefinedPrediction)
print("Refine Decision Tree algorithm results: Accuracy: {0} and recall: {1}".format(aAccuracyRef,aRecallRef))

#Predict for test case
aTestPredRef = aGridSearchResult.best_estimator_.predict_proba(aOneTestCase)
print("Refined Decision Tree prediction for the single test case: {0}".format(aTestPred))

#Save it to use in main code
with open("decisionTree", 'wb') as aDecTreeModelFile:
    pickle.dump(aGridSearchResult.best_estimator_, aDecTreeModelFile)