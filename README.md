# Smart Refresh Ml

Train ML algo to predict if a chat group may contains new messages or not. It is used to decrease the amount of call made to an API.
Everything is package in a single docker container for better isolation between project and portability.

# Container build

docker build -t smartrefreshml .

# Start Container (on the host)

You need to mount the ML test/train data in the container. I copy them in the same folder that the code on the host "C:\Code\smartRefreshMl\app\" and mount it in the container in "/app"
```
docker run -v C:\Code\smartRefreshMl\app\:/app --name smartrefreshmldev -it smartrefreshml bash
```

# Run app (in the container)

Change directory to the one where you monted the code and ML data
```
root@3a4935731942:/# cd /app
```
Then run the main python code
```
root@3a4935731942:/app# python3.7 smartRefresh.py
```