FROM python:3.7.4-buster
LABEL maintainer="charles.walker.37@gmail.com"

RUN python3.7 -m pip install keras tensorflow
# Sklearn is to use ROC curve - not mandatory but fun - and matplotlib to draw it
RUN python3.7 -m pip install sklearn matplotlib

CMD ["bash"]